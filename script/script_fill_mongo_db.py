import os
import subprocess as s

file_credential=open("db_credential_mongodb.txt", "r")

#creazione dizionario
credential = {
    "MONGODB_URI": ""
}

#riempimento dizionario
contents =file_credential.readlines()
for i in contents:
    if(i[0] != '#'):
        fill = i.split('=',1)
        credential[fill[0]] = fill[1].rstrip()

file_credential.close()

p = s.Popen(['mongoimport','--uri',credential["MONGODB_URI"],'-c','description', '--type','csv','--file','./import/description.csv','--headerline'], stdout=s.PIPE)
p.communicate()