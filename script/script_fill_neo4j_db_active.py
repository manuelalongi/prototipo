import os
import subprocess as s


def create_cypher_clean_db():
	return 'match (n) detach delete n;'

def create_cypher_fill_db(years):
	appender = []

	#TOPICS E DESCRIPTION IDENTIFIER
	appender.append('USING PERIODIC COMMIT \
		LOAD CSV WITH HEADERS FROM "file:///unique_topics.csv" AS row FIELDTERMINATOR ";" \
		CREATE (:Topic {label: coalesce(row.topic_label, "Unknown"), idTopic: coalesce(row.topic, "Unknown")});\n')

	appender.append('USING PERIODIC COMMIT \
		LOAD CSV WITH HEADERS FROM "file:///topics_descriptions.csv" AS row FIELDTERMINATOR ";" \
		CREATE (:Publication { \
			idPublication: row.publication, \
			year: row.year, \
			idTopic: row.topic \
		});\n')

	#INDEX CREATION
	appender.append('create index on :Topic(idTopic); create index on :Publication(idPublication);\n')

	#RELATIONSHIP TOPIC-DESCRIPTION
	appender.append('USING PERIODIC COMMIT \
		LOAD CSV WITH HEADERS FROM "file:///topics_descriptions.csv" AS row FIELDTERMINATOR ";" \
		MATCH (source:Topic {idTopic: row.topic}) \
		MaTCH (target:Publication {idTopic: row.topic, year: row.year}) \
		MERGE (source)-[:PUB { \
							year: row.year \
						} \
					] \
				->(target);\n')

	#LOOP ALL TOPICS RELATIONTSHIPS FILES
	for year in years:

		appender.append('USING PERIODIC COMMIT \
		LOAD CSV WITH HEADERS FROM "file:///relationships/'+year+'.csv" AS row FIELDTERMINATOR ";" \
		MATCH (source:Topic {idTopic: row.source}) \
		MATCH (target:Topic {idTopic: row.target}) \
		MERGE (source)-[:REL'+year+' { \
							weight: coalesce(row.weight, "Unknown"), \
							source_label: coalesce(row.slabel, "Unknown"), \
							target_label: coalesce(row.tlabel, "Unknown"), \
							source_id: coalesce(row.source, "Unknown"), \
							target_id: coalesce(row.target, "Unknown"), \
							year: '+year+'\
						} \
					] \
				->(target);\n')

		appender.append('MATCH (a:Topic)-[rel:REL'+year+']->(a) \
			DELETE rel;\n')

	return appender

file_credential=open("db_credential_neo4j.txt", "r")

#creazione dizionario
credential = {
    "NEO4J_URL": "",
    "NEO4J_USER": "",
    "NEO4J_PASSWORD": ""
}

#riempimento dizionario
contents =file_credential.readlines()
for i in contents:
    if(i[0] != '#'):
        fill = i.split('=',1)
        credential[fill[0]] = fill[1].rstrip()

file_credential.close()

#leggo nomi dei file delle relazioni e li parso
row_years = os.listdir("./import/relationships")
years = []
for i in row_years:
	fill = i.split('.',1)
	years.append(fill[0])

#creo file cypher per svuotare il db prima del caricamento
clean_db = open("clean_db.cypher","w+")
clean_db.write(create_cypher_clean_db())
clean_db.close()

#creo file cypher per il riempimento automatico del db
fill_db = open("fill_db.cypher","w+")
lines_to_write = create_cypher_fill_db(years)
for j in lines_to_write:
	fill_db.write(j)
fill_db.close()

cmd0 = ['./bin/neo4j ', 'start']
cmd1 = ['./bin/cypher-shell ',' -a ',credential["NEO4J_URL"], ' -u ',credential["NEO4J_USER"],' --password ' , credential["NEO4J_PASSWORD"],' --fail-fast ',' --debug ',' < clean_db.cypher ']
cmd2 = ['./bin/cypher-shell',' -a ',credential["NEO4J_URL"], ' -u ',credential["NEO4J_USER"],' --password ', credential["NEO4J_PASSWORD"],' --fail-fast ',' --debug ',' < ',' fill_db.cypher']


proc0 = os.system("".join(cmd0))
proc1 = os.system("".join(cmd1))

print("Avvio procedura di pulizia del database....")
if proc1 == 0: 
	print("Pulizia database avvenuta con successo") 
else: 
	print("Errore nella pulizia del database")


print("Avvio procedura di caricamento del database, questa operazione potrebbe richiedere minuti....")
proc2 = os.system("".join(cmd2))
if proc2 == 0: 
	print("Database riempito con successo") 
else: 
	print("Errore nel caricamento del database")
