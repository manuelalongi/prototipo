'use strict';
require('./src/config/config.js');

var express = require('express');
var app = express();

var routerDBAPI = require('./src/routes/routerDBAPI');
var routerAuthAPI = require('./src/routes/routerAuthAPI');

var port = process.env.PORT || 5000;

//used by express first
app.use(express.static('./src'));
app.use(express.static('./static'));
app.use(express.static('./lib'));

//templating engine
app.set('views', './src/views');      
app.set('view engine', 'ejs');

app.use('/api',routerDBAPI);
app.use('/auth',routerAuthAPI);

app.get('/', function (req, res){
    res.render('index', {
        title: 'Neo4j Chart'
    })
})

app.listen(port, function () {
    console.log('running server on port ' + port) 
});
