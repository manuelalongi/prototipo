# Installazione database e applicativo
In questa guida viene spiegato come installare l'applicazione:
```bash
git clone https://manuelalongi@bitbucket.org/manuelalongi/prototipo.git
```

## Requisiti
- Java versione 8
- Settaggio della variabile d'ambiente $JAVA_HOME con il path relativo alla jdk installata, per esempio:
```bash 
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_231.jdk/Contents/Home/
```
- Python3
- npm (Node Package Manager)
```bash 
curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
sudo apt-get install -y nodejs
```

## NEO4J
ineo: programma open source che permette la gestione di diverse istanze di database neo4j. Per tutte le funzionalità offerte da ineo è possibile consulatare la pagina di github al seguente link https://github.com/cohesivestack/ineo.
- scaricare e installare ineo
```bash 
curl -sSL http://getineo.cohesivestack.com | bash -s install
source ~/.bashrc
```
- creazione dei database: la porta è importante che sia diversa in quanto sarà avviato un server diverso per ogni istanza che si intende startare.
```bash 
ineo create -p7476 my_db_1
ineo create -p8486 my_db_2
```
- start del database: può essere avviata la singola istanza o tutte le istanze contemporaneamente
```bash
ineo start my_db_1
```
oppure
```bash
ineo start
```

- allo start verrà mostrato l'url dove è possibile accedere tramite browser al database. Al primo accesso vi chiederà di modificare la password di default. L'username e la password di default con cui accedere sono user:password / neoj4:neo4j

Ogni istanza verrà creata da ineo in un percorso specifico simile al seguente:
$NEO4J_HOME = {home}/.ineo/instances/{my_db_1}

- copiare il file jar contenuto nella cartella /script/apoc del progetto in ognuno dei database creati al percorso $NEO4J_HOME/plugins
- copiare nella cartella $NEO4J_HOME/conf/ il file neo4j.conf contenuto nella cartella /script del progetto

- scaricare MongoDB 4.0.12 all'indirizzo https://www.mongodb.com/download-center/community
- settaggio della cartella home di Mongodb
```bash 
export MONGO_HOME={path in cui è stato salvato}/mongodb-{os_information}-4.0.12/
```
Copiare nella cartella $MONGO_HOME/bin il file mongod.conf contenuto nella cartella /script del progetto.
- Avvio del database mongo
```bash 
$MONGO_HOME/bin/mongod --config $MONGO_HOME/bin/mongod.conf
```

## Settaggio dei file necessari per il caricamento del database neo4j
Premessa: il separatore dei campi del file csv deve essere il **punto e virgola**. Un esempio dei file descritti successivamente è posto nella cartella /script/esempio_file/.

- unique_topics.csv --> file contenente due campi [topic_label, topic] obbligatori che indicano rispettivamente la label del topic e il suo identificatore. Questo file deve contenere solo **una corrispondenza** per topic.
- topics_descriptions.csv --> file contenente i seguenti campi obbligatori [publication, year, topic, topic_label]
	- publication: indica l'identificatore della pubblicazione, l'identificatore dovrà corrispondere al campo [primary_pub_id] presente in un altro file chiamato description.csv.
	- year: l'anno di riferimento del topic
	- topic: identificatore del topic
	- topic_label: indica la label del topic
- {year}.csv --> N file che contengono le relazioni tra topic nell'anno determinato dal nome del file (per es. 2019.csv, 1995.csv ecc...). Il file contiene i seguenti campi obbligatori [source, target, weight, slabel, tlabel]
	- source: indica l'identificatore del topic sorgente della relazione
	- target: indica l'identificato del topic target della relazione
	- weight: campo numerico che indica il peso della relazione
	- slabel: indica la label del topic sorgente della relazione
	- tlabel: indica la label del topic target della relazione

## Caricamento dei dati nel DB neo4j
I file *script_fill_neo4j_db_active.py*, *db_credential_neo4j.txt*, *neo4j.conf* sono contenuti nella cartella /script del progetto.
Impostare il file system in questo modo:

- $NEO4J_HOME <-- script_fill_neo4j_db_active.py, db_credential_neo4j.txt
- $NEO4J_HOME/import/ <-- unique_topics.csv, topics_descriptions.csv
- $NEO4J_HOME/import/relationships/ <-- {year}.csv
- $NEO4J_HOME/conf/ <-- neo4j.conf

Modificare il file *db_credential_neo4j.txt* con le credenziali di accesso aggiornate.
Prima di eseguire lo script di caricamento assicurarsi che il database sia startato.
Eseguire lo script script_fill_neo4j_db_active.py, al termine il graph db sarà riempito con i dati recuperati dai dataset caricati nel cartella $NEO4J_HOME/import/.

## Settaggio dei file necessari per il caricamento del database mongodb
Premessa: il separatore del file csv deve essere la **virgola** in quanto il comando di importazione accetta solo quel formato. Un esempio dei file descritti successivamente è posto nella cartella /script/esempio_file/.

- description.csv --> campo obbligatorio [primary_pub_id]. In questo file si possono aggiungere un qualsiasi numero di campi che permettono di descrivere il topic. **Ognuno** dei campi inseriti verrà mostrato nell'applicazione quando si vorrà leggere la descrizione di un determinato topic.
	- primary_pub_id: indica l'identificatore della pubblicazione, deve corrispondere al campo [publication] del file topics_descriptions.csv
	

## Caricamento dei dati nel DB mongo
I file *script_fill_mongo_db.py*, *db_credential_mongodb.txt*, *mongod.conf* sono contenuti nella cartella /script del progetto.
Impostare il file system in questo modo:

- $MONGO_HOME <-- script_fill_mongo_db.py, db_credential_mongodb.txt
- $MONGO_HOME/import/ <-- description.csv
- $MONGO_HOME/bin/ <-- mongod.conf

Prima di eseguire lo script di caricamento assicurarsi che il database sia startato.
Eseguire lo script script_fill_mongo_db.py e al termine verrà creato un database il cui nome è quello specificato nel file db_credential_mongodb con una collection chiamata 'description' la quale conterrà le informazioni relative alla descrizione dei topic (pubblicazioni). Per il corretto funzionamento non modificare il nome della collection creata.


## Setup del progetto e esecuzione del server
#### 1. Installazione app
```bash
cd prototipo/
npm install
```

#### 2. Eseguire il server
```bash
touch .env
```
Il file .env dovrà contenere le informazioni relative ai database neo4j e mongodb da cui l'applicazione estrarrà i dati,
Il file deve essere compilato in questo modo:

	#nota: in base alla porta l'applicazione si connetterà al database neo4j richiesto.
	#decommentare un solo database alla volta, l'applicazione permette la connessione 
	#con una singola istanza. E' necessario il riavvio dell'applicazione per eseguire
	#uno switch di connessione del database.

	#DATABASE 1
    #NEO4J_URL=bolt://localhost:7687
    #NEO4J_USER=neo4j
    #NEO4J_PASSWORD=neo4j
    #MONGODB_URI=mongodb://user:password@localhost:27017/nome_database_1

	#DATABASE 2
    NEO4J_URL=bolt://localhost:7476
    NEO4J_USER=neo4j
    NEO4J_PASSWORD=neo4j
    MONGODB_URI=mongodb://user:password@localhost:27017/nome_database_2
    
    TOKEN_SALT=fdkjbnering2342f2f34g335g24g343g34g3

```bash
npm start
```

L'applicazione sarà eseguita all'indirizzo http://localhost:5000
