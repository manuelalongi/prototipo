'use strict';

var cors = require('cors'),
        express = require('express'),
        routerAuthAPI = express.Router(),
        queryStackByUser = require('./queryStackByUser'),
        auth = require('./auth');

routerAuthAPI.all('*', cors());

routerAuthAPI.route('/genToken').get(function (req, res) {
    var token = auth.generateToken();
    
    queryStackByUser.addQueryStack({queryStack: generateQueryStack(), token: token, uniqueIdGenerator: generateIdGenerator()});
    deleteExpiredQueryStack(queryStackByUser);
    
    console.log("numero di querystackbyuser: " + queryStackByUser.getAllQueryStack().length);

    res.send(
        {
            token: token
        }
    )
});

routerAuthAPI.route('/verifyToken/:token').get(function (req, res) {
    try{
        var signature = auth.verifySignature(req.params.token);
        var check = auth.checkExpiration(signature.exp);
        if (auth.checkExpiration(signature.exp)){
            res.send({
                error: "token expired, please reload the page"
            })
        }else {
            res.send(
                {
                    response: "valid token" 
                }
            )
        }
    }catch(error){
        res.send({
            error: "invalid token"
        })
    }
    
    
});

function generateQueryStack(){
    const _query = [];
    const queryStack = {
        addQuery: item => _query.push(item),
        deleteQuery: queryPosition => _query.splice(queryPosition,1),
        getQuery: params => _query.findIndex(d => d.uniqueId == params.uniqueId),
        getAllQuery: () => _query,
        getActiveQuery: () => _query.find(d => d.active == false)
      }
    
    return queryStack;
}

function generateIdGenerator(){
    let _id = 0;
    const MAX_DIFFERENT_COLORS = 19;

    const uniqueIdGenerator = {
        increaseId: () => (_id += 1) % MAX_DIFFERENT_COLORS
    }

    return uniqueIdGenerator;
}

function deleteExpiredQueryStack(queryStackByUser){
    var stack = queryStackByUser.getAllQueryStack();
    
    stack = stack.filter(function(q){
        var signature = auth.verifySignature(q.token);
        if (!auth.checkExpiration(signature.exp))
            return q;
    })

    queryStackByUser.updateQueryStackByUser(stack);
}

module.exports = routerAuthAPI;