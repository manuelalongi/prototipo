'use strict';

var cors = require('cors'),
        secret = process.env.TOKEN_SALT,
        jwt = require('jsonwebtoken');

const auth = {
    //il token scade dopo 1 ora
    generateToken: () => jwt.sign({iat: Date.now() }, secret, {expiresIn: 60000*60*1}), 
    verifySignature: token => jwt.verify(token, secret),
    checkExpiration: exp => Date.now() >= exp
      
}


module.exports = auth;
                                                
                                                