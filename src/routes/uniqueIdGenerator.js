let _id = 0;
const MAX_DIFFERENT_COLORS = 19;

const uniqueIdGenerator = {
  increaseId: () => (_id += 1) % MAX_DIFFERENT_COLORS
}

module.exports = uniqueIdGenerator;