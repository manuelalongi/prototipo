	'use strict';

	var cors = require('cors'),
			express = require('express'),
			neo4j_url = process.env.NEO4J_URL,
			neo4j_user = process.env.NEO4J_USER,
			neo4j_password = process.env.NEO4J_PASSWORD,
			routerNeo4jAPI = express.Router(),
			neo4j = require('neo4j-driver').v1,
			driver = neo4j.driver(neo4j_url, neo4j.auth.basic(neo4j_user, neo4j_password)),
			//driver = neo4j.driver('bolt://localhost:7687', neo4j.auth.basic(neo4j_user, neo4j_password)),
			//driver = neo4j.driver('bolt://localhost:7476', neo4j.auth.basic(neo4j_user, neo4j_password)),
			session = driver.session(),
			queryStackByUser = require('./queryStackByUser'),
			MongoClient = require('mongodb').MongoClient,
			MongoUri = process.env.MONGODB_URI,
			auth = require('./auth');

	routerNeo4jAPI.all('*', cors());

	routerNeo4jAPI.route('/rangeYears').get(function (req, res) {

		var resultPromise = session.run(
			/* 'match (n:Topic)-[r:PUB]->(p:Publication)\
			where r.year >= "2000" and r.year < "2019" return min(r.year) as min, max(r.year) as max' */
			'match (n:Topic)-[r:PUB]->(p:Publication)\
			return min(r.year) as min, max(r.year) as max'
		)
		.then(result => {
			session.close();
			var range = result.records.map(function(record){
				return {
					min: record._fields[0],
					max: record._fields[1]
				};
			})
			res.send(range);
			
		}).catch(error => {
			session.close();
					console.log(error);
					if (error.name = "Neo4jError")
						res.send({error: "comunication error with the database, please reload the page",
								log: error  });
					else{
						res.send({error: "errore generico",
								log: error  });
					}
		});
	})

	routerNeo4jAPI.route('/getAllQuery/:token').get(function (req, res) {
		var token = req.params.token;
		verifyToken(res,token);
				var queryStack = queryStackByUser.getQueryStack(token);
				res.send(
					{
						activeQuerys: queryStack.getAllQuery()
					}
				)
	});

	routerNeo4jAPI.route('/pushQuery/:idTopic/:label/:degree/:year/:token').get(function (req, res) {
		var token = req.params.token;
		verifyToken(res,token);
		var queryStack = queryStackByUser.getQueryStack(token);
		var uniqueIdGenerator = queryStackByUser.getUniqueIdGenerator(token);
		var uniqueQueryId = uniqueIdGenerator.increaseId();

		var query = {
			id: req.params.idTopic,
			label: req.params.label,
			degree: Number(req.params.degree),
			year: req.params.year,
			active: false,
			uniqueQueryId: uniqueQueryId,
			queryIdList: [uniqueQueryId],
			uniqueId: req.params.idTopic
		};
		
		var queryPosition = queryStack.getQuery(query);
		if (queryPosition == -1){
			queryStack.addQuery(query)
		
			res.send(
				{
					result: "query aggiunta correttamente",
					activeQuerys: queryStack.getAllQuery()
				}
			)
		}else {
			res.send(
				{
					error: "there is already a query for selected node"
				}
			)
		}
	})

	routerNeo4jAPI.route('/deleteQuery/:idTopic/:publication/:token').get(function (req, res) {
		var token = req.params.token;
		verifyToken(res,token);
		var queryStack = queryStackByUser.getQueryStack(token);

		var params = {
			id: req.params.idTopic,
			uniqueId: req.params.idTopic
		};
		var queryPosition = queryStack.getQuery(params);
		
		if (queryPosition != -1){
			var deletedQuery = queryStack.deleteQuery(queryPosition);
		
			res.send(
				{
					result: "query eliminata correttamente",
					activeQuerys: queryStack.getAllQuery(),
					deletedQuery: deletedQuery
				}
			)
		} else {
			res.send(
				{
					error: "error, query wasn't deleted"
				}
			)
		}
	})

	routerNeo4jAPI.route("/topicFreqFromYear/:year").get(function (req, res){
		var params = {
			year: req.params.year
		};
		var resultPromise = session.run(
			'match (n)-[r:PUB]->(p:Publication) where r.year = $year \
			return n.idTopic as idTopic, count(p) as freq'
		,params)
		.then(result => {
			session.close();
			var freqTopics = result.records.map(function(record){
				return {
					idTopic: record._fields[0],
					freq: record._fields[1].low
				};
			})

			res.send({freqTopics});
			
		}).catch(error => {
			session.close();
					console.log(error);
					res.send({error: "errore nell'estrazione dei dati, i parametri non sono validi",
								log: error  });
		});
			
	});

	routerNeo4jAPI.route("/weightRangeEdgesFromYear/:year").get(function (req, res){
		var params = {
			year: req.params.year,
			relationship: "REL"+req.params.year
		};

		var resultPromise = session.run(
			'match (n1:Topic)-[r]->(n2:Topic) \
			where type(r) = $relationship \
			return min(toint(r.weight)),max(toint(r.weight))'
		,params)
		.then(result => {
			session.close();
			var range = result.records.map(function(record){
				return {
					min: record._fields[0],
					max: record._fields[1]
				};
			})
			res.send(range);
			
		}).catch(error => {
			session.close();
					console.log(error);
					res.send({error: "errore nell'estrazione dei dati, i parametri non sono validi",
								log: error  });
		});
			
	});

	routerNeo4jAPI.route('/allFromYear/:year').get(function (req, res) {
		var params = {
			year: req.params.year,
			relationship: "REL"+req.params.year
		};
		var resultPromise = session.run(
			'match (n:Topic)-[r]->(p:Topic) where type(r) = $relationship\
			 return distinct(properties(n))'
		,params)
		.then(result => {
			session.close();
			var nodes = result.records.map(function(record){
				return {
					id: record._fields[0].idTopic,
					label: record._fields[0].label,
					uniqueId: record._fields[0].idTopic
				};
			})
			res.send({nodes});
			
		}).catch(error => {
			session.close();
					console.log(error);
					res.send({error: "errore nell'estrazione dei dati, i parametri non sono validi",
								log: error  });
		});
			
	});

	routerNeo4jAPI.route('/updateGraphWithActiveQuery/:token').get(function (req, res) {
		var token = req.params.token;
		verifyToken(res,token);
		var queryStack = queryStackByUser.getQueryStack(token);

		var activeQuery = queryStack.getActiveQuery();
			if(!activeQuery){
				res.send({
					nodes: [],
					links: []
				});
			}else {
						var uniqueQueryId = activeQuery.uniqueQueryId;
						activeQuery.relationship = "REL"+activeQuery.year;
						var resultPromise = session.run(
								'MATCH (n:Topic)-[r]->(p:Topic) WHERE n.idTopic = $id and type(r) = $relationship\
								CALL apoc.path.subgraphAll(n, {maxLevel:$degree,relationshipFilter: $relationship}) YIELD nodes, relationships \
								return nodes, relationships ',
							activeQuery).then(result => {
							session.close();
							var nodes = [];
							var links = [];
							
								nodes = result.records[0]._fields[0].map(function(record){
									return {label: record.properties.label,
											id: record.properties.idTopic, 
											year: activeQuery.year,
											uniqueId: record.properties.idTopic,
											uniqueQueryId: uniqueQueryId,
											queryIdList: [uniqueQueryId]
										};
								}) 
								
								links = result.records[0]._fields[1].map(function(record){
									return {
											source: record.properties.source_id,
											target: record.properties.target_id,
											uniqueQueryId: uniqueQueryId,
											weight: record.properties.weight
										};
								}) 
							
							res.send({
								nodes: nodes,
								links: links
							}); 
						}).catch(error => {
							session.close();
									console.log(error);
									res.send({error: "errore nell'estrazione dei dati",
											log: error  });
						}).finally(function(){
							session.close();
							activeQuery.active = true;
						});
		}
	});

	routerNeo4jAPI.route('/updateGraphWhenChangeYear/:year/:token').get(function (req, res) {
		var token = req.params.token;
		verifyToken(res,token);
		var queryStack = queryStackByUser.getQueryStack(token);

		var querys = queryStack.getAllQuery();
		if (querys.length == 0){
			res.send([]);
		}
		
		var response = [];
		for (let i=0; i<querys.length; i++){
			querys[i].year = req.params.year;
			querys[i].relationship = "REL"+req.params.year;
			var resultPromise = session.run(
				'MATCH (n:Topic)-[r]->(p:Topic) WHERE n.idTopic = $id and type(r) = $relationship \
								CALL apoc.path.subgraphAll(n, {maxLevel:$degree,relationshipFilter: $relationship}) YIELD nodes, relationships \
								return nodes, relationships ', querys[i]
			)
			.then(result => {
				if (result.records.length == 0){
					response.push({nodes: [], links: []});
				}else{
					var nodes = result.records[0]._fields[0].map(function(record){
						return {label: record.properties.label,
								id: record.properties.idTopic, 
								year: querys[i].year,
								uniqueId: record.properties.idTopic,
								uniqueQueryId: querys[i].uniqueQueryId,
								queryIdList: [querys[i].uniqueQueryId]
							};
					}) 
					
					var links = result.records[0]._fields[1].map(function(record){
						return {
								source: record.properties.source_id,
								target: record.properties.target_id,
								uniqueQueryId: querys[i].uniqueQueryId,
								weight: record.properties.weight
							};
					}) 
					response.push({nodes: nodes, links: links});
				}

				if (response.length == querys.length){
					res.send(response);
				}
				
			}).catch(error => {
				session.close();
						console.log(error);
						res.send({error: "errore nell'estrazione dei dati",
									log: error  });
			});
		}
	});

	routerNeo4jAPI.route('/getPublicationsFromTopic/:idTopic/:year').get(function (req, res) {
		var params = {
			idTopic: req.params.idTopic,
			year: req.params.year
		}
		
		var resultPromise = session.run(
			'match (n:Topic)-[r:PUB]->(p:Publication) \
			 where n.idTopic = $idTopic and p.year = $year return p', 
		params)
		.then(result => {
			session.close();
			var publications = result.records.map(function(record){
				return Number(record._fields[0].properties.idPublication);
			});

			MongoClient.connect(MongoUri, function (err, db) {
				var collection = db.collection('description');
				collection.find( {primary_pub_id: { $in: publications }}, 
					{fields: {
						_id: 0, 
						primary_pub_id: 0
                	}}).toArray(function (err, results) {
                    if (err) {
                        console.error(err);
                        res.statusCode = 500;
                        res.send({
                            error: err.code
                        });
					}
						
						let response = [];
						//Authors logic
						/* for( let i=0; i<results.length; i++){
							let index = response.findIndex(function(d){
								return d.publicationId == results[i].primary_pub_id;
							});

							if (index != -1){
								response[index].autors.push({
									a_name: results[i].a_name,
									a_surname: results[i].a_surname,
									role: results[i].role,
									area: results[i].comp_area
								})
							}else {
								response.push({
									publicationId: results[i].primary_pub_id,
									title: results[i].title,
									year: results[i].year,
									sector: results[i].ssd_x,
									language: results[i].language,
									abstract: results[i].abstract,
									autors: [{
										a_name: results[i].a_name,
										a_surname: results[i].a_surname,
										role: results[i].role,
										area: results[i].comp_area
									}]
								})
							}
						} */
                        res.send(results);
                    });
			})

		}).catch(error => {
			session.close();
			console.log(error);
			res.send({error: "errore nell'estrazione dei dati",
						log: error  });
		});
	})

	function verifyToken(res,token){
		try{
			var signature = auth.verifySignature(token);
			if (auth.checkExpiration(signature.exp)){
				res.send({
					token_error: "expired token, please reload the page",
					activeQuerys: []
				});
				return;
			} 
		} catch (error){
			let pos = queryStackByUser.getQueryStackPosition(token);
			queryStackByUser.deleteQueryStack(pos);
			res.send({
				token_error: "invalid token or expired, please reload the page",
				activeQuerys: [],
				nodes: [],
				links: []
			})
		}
	}
	module.exports = routerNeo4jAPI;
													
													