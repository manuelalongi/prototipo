var _queryByUser = [];

const queryStackByUser = {
  addQueryStack: params => _queryByUser.push({queryStack: params.queryStack, token: params.token, uniqueIdGenerator: params.uniqueIdGenerator}),
  getQueryStack: token => (_queryByUser.find(d => d.token == token)).queryStack,
  getQueryStackPosition: token => _queryByUser.findIndex(d => d.token == token),
  deleteQueryStack: queryStackPosition => _queryByUser.splice(queryStackPosition,1),
  getAllQueryStack: () => _queryByUser,
  updateQueryStackByUser: queryByUser => _queryByUser = queryByUser,
  getUniqueIdGenerator: token => (_queryByUser.find(d => d.token == token)).uniqueIdGenerator
}

module.exports = queryStackByUser;