function makePlayButton(moving){
    var playButton = d3.select("#slider")
      .append("button").attr("id","play-button")
      .text("Play");

    playButton.on("click",clickPlayButton);
    return playButton;
}

function makePushQueryButton(){
    var pushQueryButton = d3.select("#push-query");
    pushQueryButton.on("click",clickPushQueryButton);
    return pushQueryButton;
}

function makeRemoveQueryButton(row){
        row.append("button")
            .classed("close","true")
            .html('<span aria-hidden="true">&times;</span>')
            .attr("float", "right")
            .attr("vertical-align","middle")
            .attr("background-color","red")
            .attr("aria-label","Close")
            .attr("value",function(d){return d.uniqueQueryId})
            .on("click",function(d){
                var urlDeleteQuery = "/api/deleteQuery/" + d.id + "/" +d.publication+ "/" +token;
                d3.json(urlDeleteQuery, function(error, data){
                  if (error){
                    alert("There is an error, please reload the page");
                  } else {
                    if (data.token_error) { 
                      alert(data.token_error);
                    }else{
                      //saving active querys to show it graphiacally
                      queryList = data.activeQuerys;

                      //remove nodes and links of deletedQuery
                      filteredDataNodes = filteredDataNodes.filter(function(node) {
                        let i = node.queryIdList.findIndex(function(n){return n == data.deletedQuery[0].uniqueQueryId});
                        if(i != -1) {node.queryIdList.splice(i,1);}
                        if (node.queryIdList.length > 0)
                          return node;
                      })
                      filteredDataLinks = filteredDataLinks.filter(function(link) {
                        return link.uniqueQueryId != data.deletedQuery[0].uniqueQueryId;
                      })
                      update(data, currentYear);

                      d3.select("#querylist").selectAll("li")
                        .data(queryList, function(d) { return d.uniqueQueryId;})
                        .exit().remove();
                      d3.select(this).exit().remove();
                      
                      updateCounterQueries(queryList);
                      fixColorsAfterDeletingNodeInMoreThanOneSubgraph();
                    }
                  }
                }) 
                
                
            }) 
}