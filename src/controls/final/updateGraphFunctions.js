
function blockOldNodesInSamePosition(backNodePosition, filteredDataNodes){  
    if(backNodePosition.length > 0){
        filteredDataNodes.map(function(n){
          var temp = backNodePosition.find(function(o){
            return o.uniqueId == n.uniqueId
          })
          if(temp){
            n.fx = temp.x;
            n.fy = temp.y;
          }
        })
      }
}

function updateColorsDomainBasedOnNumberOfQuerys(colors, queryList){
    colors.domain([0,11]);
}

function updateGraphNodes(nodes, filteredDataNodes, describeNodes){
  nodes = nodes.data(filteredDataNodes, function(d) { 
    return d.uniqueId;
  });
    nodes.exit().transition().duration(500).style("fill-opacity", 1e-6).remove();
        
    nodes = nodes.enter().append("circle")
        .attr("fill", function(d){
          return colors(d.queryIdList[0]);
        })
        .attr("r", function(d){
          return scaleCircle(d.freq);
        })
        .merge(nodes)
          .call(d3.drag()  
          .on("start", dragStarted)
          .on("drag", dragging)
          .on("end", dragEnded));
    
    nodes.append("title")
        .text(function(d) {
          return d.label;
        });

    nodes
      .on("click",mouseclicknodes)
      .on("mouseover",mouseoverNodes)
      .on("mouseleave",mouseleaveNodes);

    return nodes;
}

function updateGraphLinks(edges, filteredDataLinks){
  edges = edges.data(filteredDataLinks, function(d) { 
    return d.source +"-"+d.target; 
  });
  edges.exit().transition().duration(670).style("opacity", 1e-6).remove()
  
  edges = edges.enter().append("line")
      .style("stroke", "#D5D8DC")
      //.style("stroke", 	"#f08080")
      //.style("stroke-opacity", function(d){ return scaleLineColors(d.weight)})
      //.style("stroke", function(d){ return colorsScaleLine(d.weight)})
      .style("stroke-width", function(d){
        return scaleLine(d.weight);
      })
      .merge(edges); 

    edges.on("mouseover", function(d){
      d3.select(this).style("stroke", "black");
    })
    edges.on("mouseout", function(d){
      d3.select(this).style("stroke", "#D5D8DC");
    }) 

  return edges;
}

function updateGraphLabels(labels, filteredDataNodes){
  labels = labels.data(filteredDataNodes, function(d) {return d.uniqueId;});
    labels.exit().transition().duration(750).style("fill-opacity", 1e-6).remove();

    labels = labels.enter().append("text").classed("label",true)
          .attr("x", function(d){
              return 4+scaleCircle(d.freq)*2;
            })
          .attr("y", ".31em")
          .text(function(d) { return d.label; })
          .attr("font-size",function(d){
            return scaleCircle(d.freq);
          })
          .merge(labels);
    
    labels.on("mouseover", mouseoverLabels)
          .on("mouseleave", mouseleaveLabels);

    return labels;
}

function updateGraphSimulation(simulation, filteredDataNodes, filteredDataLinks){
  simulation
        .nodes(filteredDataNodes)
        .on("tick", ticked)
        .on("end",resetFixPosition);

    simulation.force("link")
          .links(filteredDataLinks);

    simulation.alpha(1).restart();

    return simulation;
}

function dragStarted(d) {
  if (!d3.event.active) simulation.alphaTarget(0.05).restart();
  d.fx = d.x;
  d.fy = d.y;
}

function dragging(d) {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
}

function dragEnded(d) {
  if (!d3.event.active) simulation.alphaTarget(0);
  d.fx = null;
  d.fy = null;
}

function resetFixPosition(){
  if (filteredDataNodes.length > 0){
    filteredDataNodes.map(function(node){
      node.fx = null;
      node.fy = null;
    })
  }
}

var mouseoverNodes = function(d){
  d3.select(this)
    .style("stroke","black") 
    .style("stroke-width",2)
    .style("r",scaleCircle(d.freq)*2);
}

var mouseleaveNodes = function(d) {
  d3.select(this)
    .style("r",scaleCircle(d.freq))
    .style("stroke","none"); 
}

/* var mouseclicknodes = function(d){
  d3.select("#textModal").select("p").remove();
  d3.select("#titleModal").text(d.label);
  d3.select("#subtitleModal").text("frequency of appearance in the year " + currentYear)
        let description = d3.select("#textModal").append("p");
        let url = "/api/getPublicationsFromTopic/"+d.id+"/"+currentYear;
        d3.json(url, function(error, data){
          if(error) { console.log(error)};
          let appender = "frequency of appearance in the year " + currentYear + ": " + d.freq;
          if (data.length == 0){
            appender = "there is no informations about this publication";
          }
          description.html(function(){
            for (let i=0; i<data.length; i++){
              appender = appender + 
              '<br><center><u>Publication</u></center><br>' +
              '<b>Publication title:</b> ' + data[i].title + '<br>' +
              '<b>Year: </b>' + data[i].year + '<br>' +
              '<b>Sector: </b>' + data[i].sector + '<br>' +
              '<b>Abstract: </b>' + data[i].abstract + '<br>' +
              '<b>Language: </b>' + data[i].lenguage + '<br><br>' +
              '<center><u>Authors</u></center><br>'
              for (let j=0; j<data[i].autors.length; j++){
                appender = appender + 
                '<b>author: </b>' + data[i].autors[j].a_name + ' ' + data[i].autors[j].a_surname + '<br>' +
                '<b>role and area: </b>' + data[i].autors[j].role + ' ' + data[i].autors[j].area + '<br>' ;
              }
            }

            return appender;
          });

          $("#descriptionModal").modal('show');
        })
} */

var mouseclicknodes = function(d){
  d3.select("#textModal").select("p").remove();
  d3.select("#titleModal").text(d.label);
  d3.select("#subtitleModal").text("frequency of appearance in the year " + currentYear)
        let description = d3.select("#textModal").append("p");
        let url = "/api/getPublicationsFromTopic/"+d.id+"/"+currentYear;
        d3.json(url, function(error, data){
          if(error) { console.log(error)};
          let appender = "Frequency of appearance in the year " + currentYear + ": " + d.freq + "<br><br>";
          if (data.length == 0){
            appender = "there is no informations about this publication";
          }
          description.html(function(){
            for (let i=0; i<data.length; i++){
              appender = appender + "<center><u>Publication "+(i+1)+"</u></center>";
              for (let j=0; j<Object.keys(data[i]).length; j++){
                //ogni volta stampa il campo e il suo valore in ordine
                appender = appender + "<b>" + Object.keys(data[i])[j] + ":</b>" + " " + Object.values(data[i])[j] + "<br>";
              }
              appender = appender + "<br>";
            }

            return appender;
          });

          $("#descriptionModal").modal('show');
        })
}

var mouseoverLabels = function(d){
  d3.select(this)
    .style("font-size",function(d){
      return scaleCircle(d.freq)*2;
    });
}

var mouseleaveLabels = function(d){
  d3.select(this)
    .style("font-size",function(d){
      return scaleCircle(d.freq);
    });
}


function updateSearchNodes(currentYear){
  var urlAllFromYear = '/api/allFromYear/' + currentYear;
  var freqTopicsFromYear = 'api/topicFreqFromYear/' + currentYear;
  d3.queue()
    .defer(d3.json,urlAllFromYear)
    .defer(d3.json,freqTopicsFromYear)
    .await(function(error, dataNodes, dataFreq){
      if (error) {alert("There is an error, please reload the page");}
      else {
        searchNodes = dataNodes.nodes;
        freqTopics = dataFreq.freqTopics;
        updateScaleCircleWithFreq(freqTopics);
        updateSearchList();
        updatePositionCursorSlider(handle,scaleTime,currentYear);
      }

    })
}

function updateSearchNodesWhenChangeYear(currentYear){
  var urlAllFromYear = '/api/allFromYear/' + currentYear;
  var freqTopicsFromYear = 'api/topicFreqFromYear/' + currentYear;
  d3.queue()
    .defer(d3.json,urlAllFromYear)
    .defer(d3.json,freqTopicsFromYear)
    .await(function(error, dataNodes, dataFreq){
    if (error) {alert("There is an error, please reload the page");}
    else {
      searchNodes = dataNodes.nodes;
      freqTopics = dataFreq.freqTopics;
      updateScaleCircleWithFreq(freqTopics);
      updateSearchList();
      updatePositionCursorSlider(handle,scaleTime,currentYear);
      updateGraphWhenChangeYear(currentYear);
    }

  })
}

function updateSearchList() {
  input = document.getElementById('searchInput');
  d3.select("#searchresults").selectAll("li").remove();

  d3.select("#searchresults").selectAll("li")
    .data(searchNodes, function(d) { return d.id;})
    .enter().append("li")
    .html(function(d){return "<b>"+d.label+"</b>"})
    .on("click", function(d){ 
      selectedNode = d;
      var inputText = document.getElementById("searchInput");
      if (inputText){
        inputText.value = "";
      }
      d3.select("#searchInput").attr('placeholder',d.label)
      d3.select("#searchresults").selectAll("li").remove();
    });  
}

function showQueryActive(){
  var urlQueryActive = '/api/getAllQuery/' + token;
  d3.json(urlQueryActive, function(error, data){
    if (error) { console.log(error);}
    if (data) {queryList = data.activeQuerys; }
    if (queryList.length > 0){
      var row = d3.select("#querylist").selectAll("tr")
        .data(queryList, function(d) { return d.uniqueQueryId;})
        .enter().append("tr");
            
        row.append("td")
                .html(function(d){return "topic: <br>"+ d.label + "<br>publicationId: "+ d.publication+"<br>depth:" + d.degree+"<br>"});
            
        makeRemoveQueryButton(row);
    }

  });
 
}

function updateGraphWhenChangeYear(currentYear){
  var urlUpdateGraphChangeYear = '/api/updateGraphWhenChangeYear/' + currentYear + '/'+token;
  //var urlRangeWeight = '/api/weightRangeEdgesFromYear/' + currentYear;
  d3.queue()
        .defer(d3.json,urlUpdateGraphChangeYear)
        //.defer(d3.json,urlRangeWeight)
        .await(function(error, data, rangeWeight) {

    if (error) { alert("There is an error, please reload the page");}
    if (data.token_error){
      alert(data.token_error);
      filteredDataNodes = data.nodes;
      filteredDataLinks = data.links;
    }
    /* if (rangeWeight && rangeWeight[0].min && rangeWeight[0].max){
      updateScaleLine(rangeWeight[0].min,rangeWeight[0].max);
    } */

    if (data.length > 0){
      filteredDataNodes = [];
      filteredDataLinks = [];
      
      for (let i=0; i<data.length; i++){

        for (let j=0; data && j<data[i].nodes.length; j++){
          let nodeFounded = filteredDataNodes.find(function(n){
            return n.uniqueId == data[i].nodes[j].uniqueId;
          });
          if (!nodeFounded) {
            filteredDataNodes.push(data[i].nodes[j]);
          } 
          //gestione dei nodi presenti in più sottografi
          else {
            nodeFounded.queryIdList.push(data[i].nodes[j].uniqueQueryId);
          }
        } 

        for (let k=0; k<data[i].links.length; k++){
          if (!filteredDataLinks.find(function(l){
            return l.source.uniqueId == data[i].links[k].source && l.target.uniqueId == data[i].links[k].target;
          })) {
            filteredDataLinks.push(data[i].links[k]);
          }
        }
      }
      
      filteredDataNodes.forEach(node => {
        index = freqTopics.findIndex(function (freq){
          return freq.idTopic == node.id;
        });
        
        if (index != -1){
          node.freq = freqTopics[index].freq;
        }else {
          node.freq = 1;
        }
      });
    }

    update(data, currentYear);
    fixDimensionOfNodesAndLabels();
  });
}

function fixColorsAfterDeletingNodeInMoreThanOneSubgraph(){
  d3.select("#nodes").selectAll("circle").style("fill",function(d){
    if (d.queryIdList.length > 0){
      return colors(d.queryIdList[0]);
    }
  });
}
function fixDimensionOfNodesAndLabels(){
  d3.select("#nodes").selectAll("circle").style("r",function(d){
      return scaleCircle(d.freq);
  });

  d3.select("#labels").selectAll("text").style("font-size",function(d){
    return scaleCircle(d.freq);
});
}

function updateScaleCircleWithFreq(freqTopics){
  let maxFreq = 200;
  let minFreq = 0;
  maxFreq = freqTopics.reduce((max, p) => p.freq > max.freq ? p : max);
  minFreq = freqTopics.reduce((min, p) => p.freq < min.freq ? p : min);
  scaleCircle = d3.scaleLinear().domain([minFreq.freq,maxFreq.freq]).range([9,20]);
}

function updateScaleLine(min, max){
  scaleLine = d3.scaleLinear().domain([min,max]).range([0.5,3]);
}