function clickPushQueryButton(){
  if (queryList.length < MAX_QUERY_ACTIVE){
    var e = document.getElementById("depth");
    selectedDepth = e.options[e.selectedIndex].value;
    if (selectedDepth && selectedNode){
      var urlPushQuery = "/api/pushQuery/" + selectedNode.id + "/"
        +selectedNode.label+"/"
        +selectedDepth+"/"+currentYear+"/"
        +token;
      
      d3.queue()
        .defer(d3.json,urlPushQuery)  
        .await(function(error, data){
      //d3.json(urlPushQuery, function(error, data){
        if (error){
          alert("There is an error, please reload the page");
        } else {
          if(data.token_error) {
            alert(data.token_error);
          }else {
            queryList = data.activeQuerys;
            var row = d3.select("#querylist").selectAll("li")
            .data(queryList, function(d) {return d.uniqueQueryId;})
            .enter().append("li").classed("list-group-item success",true).attr("id","queryItem")
            .attr("background-color",function(d){
              return colors(d.queryIdList[0]);
            })

            makeRemoveQueryButton(row);
            row.append("span").html(function(d){
              return '<b>topic:</b> '+ d.label + '<br>'+
                '<b>depth:</b>' + d.degree+'<br>' 
                
            })
            .attr("float","left");
              
            row.style("background-color", function(d){
              return colors(d.queryIdList[0])})

            updateCounterQueries(queryList);
            clean();
            updateGraphButton();
          }
        }
      })
        
    } else {
      alert("Before push select a topic");
    }
  } else {
    alert("The maximum number of active queries has been reached, please remove a query before push the next one");
  }
  updateSearchNodes(currentYear);
}

function filterNodes() {
  // Declare variables
  var input;
  input = document.getElementById('searchInput');
  d3.select("#searchresults").selectAll("li").remove();

  var selectedNodeLabel = input.value.toLowerCase();

  d3.select("#searchresults").selectAll("li")
    
    .data(searchNodes.filter(function(d){
      return d.label.indexOf(selectedNodeLabel) != -1;
    }), function(d) { return d.label;})
    
    .enter().append("li")
    .html(function(d){return "<b>"+d.label+"</b>"})
    .on("click", function(d){ 
      selectedNode = d;
      var inputText = document.getElementById("searchInput");
      if (inputText){
        inputText.value = "";
      }
      d3.select("#searchInput").attr('placeholder',d.label)
      d3.select("#searchresults").selectAll("li").remove();
    });  
}

function clean(){

  d3.select("#searchInput").attr('placeholder',"");
  selectedNode = "";

}

function updateGraphButton(){
  var urlUpdateGraph = "/api/updateGraphWithActiveQuery"+"/"+token;  
  
  d3.queue()
    .defer(d3.json,urlUpdateGraph)
    .await(function(error,data){
  //d3.json(urlUpdateGraph, function(error, data){
      if (error) { 
        alert("There is an error, please reload the page");
      } else {
        if (data.token_error) { 
          alert(data.token_error);
        } else {
          removeDuplicateNodesAndLinksAndAddFreq(data,filteredDataNodes,filteredDataLinks);
          update(data,currentYear);
        }
      }
  });
  
}

function removeDuplicateNodesAndLinksAndAddFreq(data,filteredDataNodes, filteredDataLinks){
  for (let i=0; data && i<data.nodes.length; i++){
    let nodeFounded = filteredDataNodes.find(function(n){
      return n.uniqueId == data.nodes[i].uniqueId;
    });
    if (!nodeFounded) {
      filteredDataNodes.push(data.nodes[i]);
    } 
    //gestione dei nodi presenti in più sottografi
    else {
      nodeFounded.queryIdList.push(data.nodes[i].uniqueQueryId);
    }
  } 

  for (let i=0; data && i<data.links.length; i++){
    if (!filteredDataLinks.find(function(l){
      return l.source == data.links[i].source && l.target == data.links[i].target
        //|| l.source.uniqueId == data.links[i].source && l.target.uniqueId == data.links[i].target;
    })) {
      filteredDataLinks.push(data.links[i]);
    }
  }

  filteredDataNodes.forEach(node => {
    index = freqTopics.findIndex(function (freq){
      return freq.idTopic == node.id;
    });

    if (index != -1){
      node.freq = freqTopics[index].freq;
    } else {
      node.freq = 1;
    }

  });
}

function updateCounterQueries(queryList){
  if (queryList){
    d3.select("#query-counter").text(queryList.length);
  }

}

