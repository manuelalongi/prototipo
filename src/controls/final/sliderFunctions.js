    function draggedSlider(value) {
        var x = scaleTime.invert(value), index = null, midPoint, cx, xVal;
        if(step) {
            // if step has a value, compute the midpoint based on range values and reposition the slider based on the mouse position
            for (var i = 0; i < rangeValues.length - 1; i++) {
                if (x >= rangeValues[i] && x <= rangeValues[i + 1]) {
                    index = i;
                    break;
                }
            }
            midPoint = (rangeValues[index] + rangeValues[index + 1]) / 2;
            if (x < midPoint) {
                cx = scaleTime(rangeValues[index]);
                xVal = rangeValues[index];
            } else {
                cx = scaleTime(rangeValues[index + 1]);
                xVal = rangeValues[index + 1];
            }

            if (currentYear != xVal) {
            currentYear = xVal;
            updateSearchNodesWhenChangeYear(currentYear);
            }
            
        } else {
            // if step is null or 0, return the drag value as is
            cx = scaleTime(x);
            xVal = x.toFixed(3);
        }
        // use xVal as drag value
        handle.attr('cx', cx);
    }

    function rangeCalc(start, end) {
        var ans = [];
        for (let i = start; i <= end; i++) {
            ans.push(i);
        }
        return ans;
    }

    function clickPlayButton(){
        if (playButton.text() == "Pause") {
        moving = false;
        clearInterval(timer);
        playButton.text("Play");
        } else {
        moving = true;
        //set timer on 2 seconds
        playButton.text("Pause");
        timer = setInterval(play, 2000);
        }
    }

    function play() { 
        if (currentYear > range[1]) {
            currentYear = range[0];
            updateSearchNodesWhenChangeYear(currentYear);
        }else{
            currentYear = currentYear + 1;
            updateSearchNodesWhenChangeYear(currentYear);
            
        }
    }

    function updatePositionCursorSlider(handle,scaleTime, year){
        handle.attr("cx", scaleTime(year));
    }