function makeSliderSvg(marginSlider){
    var svgSlider = d3.select("#slider").append("svg").classed("svgslider",true)
        .attr("preserveAspectRatio", "xMidYMid meet")
        .attr("viewBox", "0 0 1300 60")
      .append("g")
        .attr("transform", "translate(" + marginSlider.left + "," + marginSlider.top + ")");

    return svgSlider;
}

function makeSliderScaleTime(range, marginSlider){
    var scaleTime = d3.scaleLinear()
        .domain(range)
        .range([marginSlider.left ,widthSlider - marginSlider.left - marginSlider.right])
        .clamp(true); 

    return scaleTime;
}

function makeSliderRangeValues(range){
    return d3.range(range[0], range[1], step || 1).concat(range[1]);
}

function makeSliderXAxis(scaleTime, rangeValues){
    return d3.axisBottom(scaleTime).tickValues(rangeValues).tickFormat(function (d) {
        return d;
    });
}

function makeSliderLine(svgSlider, scaleTime, range){
    var slider = svgSlider.append("g")
        .attr("class", "slider");

    slider.append("line")
        .attr("class", "track")
        .attr("x1", scaleTime.range()[0])
        .attr("x2", scaleTime.range()[1])
      .select(function() { return this.parentNode.appendChild(this.cloneNode(true)); })
        .attr("class", "track-inset")
      .select(function() { return this.parentNode.appendChild(this.cloneNode(true)); })
        .attr("class", "track-overlay")
        .call(d3.drag()
            .on("start.interrupt", function() { slider.interrupt(); })
            .on("start drag", function() {
              draggedSlider(d3.event.x);
            })
        );

    slider.insert("g", ".track-overlay")
        .attr("class", "ticks")
        .attr("transform", "translate(0," + 18 + ")")
      .selectAll("text")
        .data(rangeCalc(range[0],range[1]))
        .enter()
        .append("text")
        .attr("x", scaleTime)
        .attr("y", 10)
        .attr("text-anchor", "middle")
        .text(function(d) { 
          return d;
        });

    return slider;
}

function makeSliderHandleAsCircle(slider){
    var handle = slider.insert("circle", ".track-overlay")
        .attr("class", "handle")
        .attr("r", 9); 

    return handle;
}