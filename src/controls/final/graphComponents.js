function makeGraphSvg(margin){
    var svg = d3.select("#vis").append("svg").classed("graph rounded border",true)
        .attr("preserveAspectRatio", "xMidYMid meet")
        .attr("viewBox", "0 0 1200 1000")
        
        .attr("id", "svgGraph")
        .call(d3.zoom().on("zoom", function(){
          svg.attr("transform", d3.event.transform)
        }))
      .append("g")
        

    return svg;
}

function makeGraphEdges(svg, filteredDataLinks){
  /* var edges = svg.append("g").attr("class", "links")
    .selectAll("line")
    .data(filteredDataLinks) */
    var edges = svg.append("g").attr("class", "links")
    .selectAll("path")
    .data(filteredDataLinks)

    return edges;
}

function makeGraphNodes(svg, filteredDataNodes){
    var nodes = svg.append("g")
      .attr("class", "nodes")
      .attr("id","nodes")
      .selectAll("circle")
      .data(filteredDataNodes)

    nodes.append("title")
    .text(function(d) {
    return d.label;
    });  

    return nodes;
}

function makeGraphLabels(svg, filteredDataNodes){
    var labels = svg.append("g")
      .attr("class", "labels")
      .attr("id","labels")
      .selectAll("text")
      .data(filteredDataNodes)

      return labels;
}

function makeGraphSimulation(filteredDataNodes, filteredDataLinks, nodes, edges, labels){
    var simulation = d3.forceSimulation()
      .force("link", d3.forceLink().id(function(d) { return d.uniqueId; }).strength(0.5))
      .force("charge", d3.forceManyBody())
      .force("center", d3.forceCenter(width / 2, heightGraph / 2))
      .force("collide", d3.forceCollide(function(d){ return scaleCircle(d.freq) * 4}));

    simulation.velocityDecay([0.8]);

    //Every time the simulation "ticks", this will be called
    simulation
      .nodes(filteredDataNodes)
      .on("tick", ticked)
      .on("end",resetFixPosition);

    simulation.force("link")
        .links(filteredDataLinks);
      
    return simulation;
}

function ticked() {
    //speed up node positioning
    for (let i = 0; i < 4; i++) {
      simulation.tick();
    } 

     nodes
        .attr("transform", function(d) {
          return "translate(" + d.x + "," + d.y + ")";
        });  
    
    edges
    .attr("x1", function(d) { return d.source.x; })
    .attr("y1", function(d) { return d.source.y; })
    .attr("x2", function(d) { return d.target.x; })
    .attr("y2", function(d) { return d.target.y; }); 

    labels
        .attr("transform", function(d) {
          return "translate(" + d.x + "," + d.y + ")";
        });
}