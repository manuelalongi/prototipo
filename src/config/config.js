if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}
module.exports = {
  endpoint: process.env.API_URL
};